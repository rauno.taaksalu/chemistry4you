package RaunoProject.C4Y;

import RaunoProject.C4Y.Entity.QuizElement;
import RaunoProject.C4Y.Repository.QuizRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class C4YApplication {

	public C4YApplication(QuizRepository quizRepository) {
		this.quizRepository = quizRepository;
}

	public static void main(String[] args) {
		SpringApplication.run(C4YApplication.class, args);
	}

	private final QuizRepository quizRepository;
	@PostConstruct
	public void run() {
		quizRepository.save(new QuizElement(1L, "H", "Hydrogen"));
		quizRepository.save(new QuizElement(2L, "He", "Helium"));
		quizRepository.save(new QuizElement(3L,"Li", "Lithium"));
		quizRepository.save(new QuizElement(4L,"Be", "Beryllium"));
		quizRepository.save(new QuizElement(5L,"B", "Boron"));
		quizRepository.save(new QuizElement(6L, "C", "Carbon"));
		quizRepository.save(new QuizElement(7L,"N", "Nitrogen"));
		quizRepository.save(new QuizElement(8L,"O", "Oxygen"));
		quizRepository.save(new QuizElement(9L,"F", "Fluorine"));
		quizRepository.save(new QuizElement(10L,"Ne", "Neon"));
		quizRepository.save(new QuizElement(11L,"Na", "Sodium"));
		quizRepository.save(new QuizElement(12L,"Mg", "Magnesium"));
		quizRepository.save(new QuizElement(13L,"Al", "Aluminum"));
		quizRepository.save(new QuizElement(14L,"Si", "Silicon"));
		quizRepository.save(new QuizElement(15L,"P", "Phosphorus"));
		quizRepository.save(new QuizElement(16L,"S", "Sulfur"));
		quizRepository.save(new QuizElement(17L,"Cl", "Chlorine"));
		quizRepository.save(new QuizElement(18L,"Ar", "Argon"));
		quizRepository.save(new QuizElement(19L,"K", "Potassium"));
		quizRepository.save(new QuizElement(20L,"Ca", "Calcium"));
		quizRepository.save(new QuizElement(21L,"Sc", "Scandium"));
		quizRepository.save(new QuizElement(22L,"Ti", "Titanium"));
		quizRepository.save(new QuizElement(23L,"V", "Vanadium"));
		quizRepository.save(new QuizElement(24L,"Cr", "Chromium"));
		quizRepository.save(new QuizElement(25L,"Mn", "Manganese"));
		quizRepository.save(new QuizElement(26L,"Fe", "Iron"));
		quizRepository.save(new QuizElement(27L,"Co", "Cobalt"));
		quizRepository.save(new QuizElement(28L,"Ni", "Nickel"));
		quizRepository.save(new QuizElement(29L,"Cu", "Copper"));
		quizRepository.save(new QuizElement(30L,"Zn", "Zinc"));
		quizRepository.save(new QuizElement(31L,"Ga", "Gallium"));
		quizRepository.save(new QuizElement(32L,"Ge", "Germanium"));
		quizRepository.save(new QuizElement(33L,"As", "Arsenic"));
		quizRepository.save(new QuizElement(34L,"Se", "Selenium"));
		quizRepository.save(new QuizElement(35L,"Br", "Bromine"));
		quizRepository.save(new QuizElement(36L,"Kr", "Krypton"));
		quizRepository.save(new QuizElement(37L,"Rb", "Rubidium"));
		quizRepository.save(new QuizElement(38L,"Sr", "Strontium"));
		quizRepository.save(new QuizElement(39L,"Y", "Yttrium"));
		quizRepository.save(new QuizElement(40L,"Zr", "Zirconium"));
		quizRepository.save(new QuizElement(41L,"Nb", "Niobium"));
		quizRepository.save(new QuizElement(42L,"Mo", "Molybdenum"));
		quizRepository.save(new QuizElement(43L,"Tc", "Technetium"));
		quizRepository.save(new QuizElement(44L,"Ru", "Ruthenium"));
		quizRepository.save(new QuizElement(45L,"Rh", "Rhodium"));
		quizRepository.save(new QuizElement(46L,"Pd", "Palladium"));
		quizRepository.save(new QuizElement(47L,"Ag", "Silver"));
		quizRepository.save(new QuizElement(48L,"Cd", "Cadmium"));
		quizRepository.save(new QuizElement(49L,"In", "Indium"));
		quizRepository.save(new QuizElement(50L,"Sn", "Tin"));
		quizRepository.save(new QuizElement(51L,"Sb", "Antimony"));
		quizRepository.save(new QuizElement(52L,"Te", "Tellurium"));
		quizRepository.save(new QuizElement(53L,"I", "Iodine"));
		quizRepository.save(new QuizElement(54L,"Xe", "Xenon"));
	}
	CommandLineRunner run1(QuizRepository quizRepository) {
		return args -> {
			};
	}
}

