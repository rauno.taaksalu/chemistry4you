package RaunoProject.C4Y.Repository;

import RaunoProject.C4Y.Entity.User;
import jakarta.persistence.metamodel.SingularAttribute;
import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;
import java.util.Optional;

public interface UserRepository extends JpaRepository <User, Long> {
    User findByName(String name);

    void deleteById(SingularAttribute<AbstractPersistable, Serializable> id);

    Optional<User> findOneByEmailAndPassword(String email, String password);

    User findById(long id);
}
