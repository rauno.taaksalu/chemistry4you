package RaunoProject.C4Y.Repository;

import RaunoProject.C4Y.Entity.QuizElement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface QuizRepository extends JpaRepository<QuizElement, Long> {

    List<QuizElement> findAll();

}
