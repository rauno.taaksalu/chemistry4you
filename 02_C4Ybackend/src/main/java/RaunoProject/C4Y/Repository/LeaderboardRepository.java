package RaunoProject.C4Y.Repository;

import RaunoProject.C4Y.Entity.Leader;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LeaderboardRepository extends JpaRepository<Leader, Long> {
    List<Leader> findTop10ByOrderByScoreDesc();
}
