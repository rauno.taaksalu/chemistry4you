package RaunoProject.C4Y.Enum;

import lombok.Getter;

@Getter
public enum ElementGroup {
    GROUP_ONE("1"),
    GROUP_TWO("2"),
    GROUP_THREE("3"),
    GROUP_FOUR("4"),
    GROUP_FIVE("5"),
    GROUP_SIX("6"),
    GROUP_SEVEN("7"),
    GROUP_EIGHT("8"),
    GROUP_NINE("9"),
    GROUP_TEN("10"),
    GROUP_ELEVEN("11"),
    GROUP_TWELVE("12"),
    GROUP_THIRTEEN("13"),
    GROUP_FOURTEEN("14"),
    GROUP_FIFTEEN("15"),
    GROUP_SIXTEEN("16"),
    GROUP_SEVENTEEN("17"),
    GROUP_EIGHTEEN("18");

    private final String group;

    ElementGroup(String group) {
        this.group = group;
    }
}



