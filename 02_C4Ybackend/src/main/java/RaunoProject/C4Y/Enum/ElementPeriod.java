package RaunoProject.C4Y.Enum;

import lombok.Getter;

@Getter
public enum ElementPeriod {
    PERIOD_ONE("1"),
    PERIOD_TWO("2"),
    PERIOD_THREE("3"),
    PERIOD_FOUR("4"),
    PERIOD_FIVE("5"),
    PERIOD_SIX("6"),
    PERIOD_SEVEN("7");

    private final String period;

    ElementPeriod(String period) {
        this.period = period;

    }
}