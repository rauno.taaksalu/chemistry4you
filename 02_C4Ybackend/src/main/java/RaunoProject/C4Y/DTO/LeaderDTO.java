package RaunoProject.C4Y.DTO;


import RaunoProject.C4Y.Entity.Leader;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LeaderDTO {

    private Long id;
    private String userName;
    private Integer score;

    public static LeaderDTO fromEntity(Leader leader) {
        return LeaderDTO.builder()
                .id(leader.getId())
                .userName(leader.getUserName())
                .score(leader.getScore())
                .build();
    }
}
