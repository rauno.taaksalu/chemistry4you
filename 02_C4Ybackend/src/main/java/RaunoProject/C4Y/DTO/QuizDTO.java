package RaunoProject.C4Y.DTO;

import RaunoProject.C4Y.Entity.QuizElement;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class QuizDTO {


    private Long id;
    private String elementSymbol;
    private String elementName;

    public static QuizDTO fromEntity(QuizElement element) {
        return QuizDTO.builder()
                .id(element.getId())
                .elementSymbol(element.getElementSymbol())
                .elementName(element.getElementName())
                .build();
    }
}
