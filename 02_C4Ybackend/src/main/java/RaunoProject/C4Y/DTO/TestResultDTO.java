package RaunoProject.C4Y.DTO;

import lombok.Data;

import java.util.List;

@Data
public class TestResultDTO {

    private List<QuizDTO> elements;
    private List<String> answers;
}
