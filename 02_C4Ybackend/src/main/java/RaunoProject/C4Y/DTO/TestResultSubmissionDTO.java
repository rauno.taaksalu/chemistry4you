package RaunoProject.C4Y.DTO;

import lombok.Data;

@Data
public class TestResultSubmissionDTO {

    private String userName;
    private TestResultDTO testResult;
}

