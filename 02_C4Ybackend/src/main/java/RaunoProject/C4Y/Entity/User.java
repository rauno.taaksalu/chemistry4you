package RaunoProject.C4Y.Entity;


import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    @Column(name = "id",unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    @Column (name = "id_code", length = 11)
    private String idCode;


    @NotEmpty(message = "You have to input a name!")
    @Column (name = "name", length = 255)
    private String name;

    @Size(min = 3, max = 30, message = "The password should be between 3 - 30 characters!")
    @Column (name = "password", length = 255)
    private String password;

    @NotEmpty(message = "You have to input your email!")
    @Column (name = "email", length = 255)
    private String email;

    @Column (name = "school", length = 255)
    private String school;


    @Min(1)
    @Max(99)
    @Column (name = "age")
    private Integer age;

}
