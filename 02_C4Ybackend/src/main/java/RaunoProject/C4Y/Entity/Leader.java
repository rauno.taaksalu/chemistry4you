package RaunoProject.C4Y.Entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Leader {
//    TODO! Add some validation. Add Column annotations.
    @Id
    @GeneratedValue
    private Long id;

    private String userName;

    private Integer score;
}
