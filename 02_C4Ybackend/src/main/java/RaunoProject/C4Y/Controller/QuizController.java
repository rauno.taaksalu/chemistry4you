package RaunoProject.C4Y.Controller;

import RaunoProject.C4Y.DTO.QuizDTO;
import RaunoProject.C4Y.DTO.TestResultSubmissionDTO;
import RaunoProject.C4Y.Service.QuizService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/quiz")
@RequiredArgsConstructor
public class QuizController {

    private final QuizService quizService;

    @GetMapping
    public ResponseEntity<List<QuizDTO>> getRandomQuestion() {
        return ResponseEntity.ok(quizService.getQuizElements());
    }

    @PostMapping
    public ResponseEntity<Integer> submitSolution(@RequestBody TestResultSubmissionDTO testResultSubmissionDTO) {
        return ResponseEntity.ok(
                quizService.saveSubmission(testResultSubmissionDTO.getUserName(), testResultSubmissionDTO.getTestResult())
        );
    }


}
