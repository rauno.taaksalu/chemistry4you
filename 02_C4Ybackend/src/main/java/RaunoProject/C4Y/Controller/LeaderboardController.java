package RaunoProject.C4Y.Controller;


import RaunoProject.C4Y.DTO.LeaderDTO;
import RaunoProject.C4Y.Service.LeaderboardService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping ("/api/leaderboard")
@RequiredArgsConstructor

public class LeaderboardController {

    private final LeaderboardService leaderBoardService;

    @GetMapping
    public List<LeaderDTO> getLeaders() {
        return leaderBoardService.getTop();
    }

}
