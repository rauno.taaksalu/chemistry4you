package RaunoProject.C4Y.Controller;

import RaunoProject.C4Y.DTO.Response;
import RaunoProject.C4Y.Entity.User;
import RaunoProject.C4Y.Service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Map;


//TODO! Future implementation

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping("/save")
    public ResponseEntity<Response> saveUsers(@RequestBody @Valid User user) {
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(LocalDateTime.now())
                        .data(Map.of("users", userService.createUser(user)))
                        .message("User created ").build()
        );
    }
    @PostMapping("/delete/{id}")
    public ResponseEntity<Response> deleteUser(@PathVariable("id") User id) {
        return ResponseEntity.ok(
                Response.builder()
                        .timeStamp(LocalDateTime.now())
                        .data(Map.of("users", userService.deleteUser(id)))
                        .message("User deleted ").build()

        );
}




























//    @GetMapping("/list")
//    public ResponseEntity<Response> getUsers() {
//        return (ResponseEntity<Response>) ResponseEntity.ok(
//                Response.builder()
//                        .timeStamp(LocalDateTime.now())
//                        .data(Map.of("users", userService.list(30)))
//                        .message("Users retrived")
//        );
//    }
//
//    @PostMapping("/save")
//    public ResponseEntity<Response> saveUsers(@RequestBody @Valid User user) {
//        return (ResponseEntity<Response>) ResponseEntity.ok(
//                Response.builder()
//                        .timeStamp(LocalDateTime.now())
//                        .data(Map.of("users", userService.createUser(user)))
//                        .message("User created ")
//        );
//    }
//
//    @PostMapping("/delete/{id}")
//    public ResponseEntity<Response> deleteUser(@PathVariable("id") User id) {
//        return ResponseEntity.ok(
//                Response.builder()
//                .timeStamp(LocalDateTime.now())
//                .data(Map.of("users", userService.deleteUser(id)))
//                .message("User deleted ")
//
//        );
//
//
//    }
}
