package RaunoProject.C4Y.Service;


import RaunoProject.C4Y.Entity.User;
import RaunoProject.C4Y.Repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

import static org.springframework.data.jpa.domain.AbstractPersistable_.id;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public User createUser(User user) {
        log.info("Saving new user: {}", user.getId());
        return userRepository.save(user);
    }

    public User updateUser(User user) {
        log.info("Updating user: {}", user.getId());
        return userRepository.save(user);
    }
    public Collection<User> list(int limit) {
        log.info("Showing all users: ");
        return userRepository.findAll(PageRequest.of(0,limit)).toList();
    }

    public Boolean deleteUser(User user) {
        log.info("deleting user: {}", id);
        userRepository.deleteById(id);
        return Boolean.TRUE;
    }
}


