package RaunoProject.C4Y.Service;

import RaunoProject.C4Y.DTO.LeaderDTO;
import RaunoProject.C4Y.Repository.LeaderboardRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class LeaderboardService {

    private final LeaderboardRepository leaderBoardRepository;

    public List<LeaderDTO> getTop() {
        log.info("Getting top 10 scores");
        var result = leaderBoardRepository.findTop10ByOrderByScoreDesc();
        return result.stream().map(LeaderDTO::fromEntity).toList();
    }
}
