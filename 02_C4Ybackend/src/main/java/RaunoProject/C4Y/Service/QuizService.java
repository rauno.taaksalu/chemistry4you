package RaunoProject.C4Y.Service;

import RaunoProject.C4Y.DTO.QuizDTO;
import RaunoProject.C4Y.Entity.Leader;
import RaunoProject.C4Y.Entity.QuizElement;
import RaunoProject.C4Y.DTO.TestResultDTO;
import RaunoProject.C4Y.Repository.LeaderboardRepository;
import RaunoProject.C4Y.Repository.QuizRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class QuizService {

    private final QuizRepository quizRepository;
    private final LeaderboardRepository leaderboardRepository;

    public List<QuizDTO> getQuizElements() {
        List<QuizElement> result = new ArrayList<>();
        List<QuizElement> allQuizElements = quizRepository.findAll();
        Random rand = new Random();
        for (int i = 0; i < 10; i++) {
            result.add(allQuizElements.get(rand.nextInt(allQuizElements.size())));
        }

        return result.stream().map(QuizDTO::fromEntity).toList();
    }

    public Integer saveSubmission(String userName, TestResultDTO testResultDTO) {
        var score = calculateScore(testResultDTO);
        Leader leader = Leader.builder()
                .userName(userName)
                .score(score)
                .build();
        leaderboardRepository.save(leader);
        return score;
    }

    private Integer calculateScore(TestResultDTO testResultDTO) {
        int score = 0;
        for (int i = 0; i < testResultDTO.getAnswers().size(); i++) {
            String correctAnswer = testResultDTO.getElements().get(i).getElementName();
            String submittedAnswer = testResultDTO.getAnswers().get(i);
            if (correctAnswer.equalsIgnoreCase(submittedAnswer)) {
                score++;
            }
        }
        return score;
    }

}
