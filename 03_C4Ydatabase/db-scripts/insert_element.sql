use chemistry4you;

-- import the file

load data local infile '/Users/kermokeerma/IdeaProjects/chemistry4you/01-additional-files/element.csv'
into table element_category
fields terminated by ','
ignore 1 rows;

insert into element (atomic_number, atomic_mass, element_group, element_name, element_period, element_phase, element_type,
number_of_electrons, number_of_neutrons, number_of_protons, number_of_shells, number_of_valence, symbol, category_id)
values (1, 1.007, 1, "Hydrogen", 1, "gas", "nonmetal", 1, 0, 1, 1, 1, "H", 8);

select * from element;