use chemistry4you;

-- import the file

load data local infile '/Users/kermokeerma/IdeaProjects/chemistry4you/01-additional-files/element_category.numbers'
into table element_category
fields terminated by ','
ignore 1 rows;

insert into element_category (id, category_name)
values
(1, "alkali metals"),
(2, "alkaline erath metals"),
(3, "lanthanoids"),
(4, "aktinioids"),
(5, "transition metals"),
(6, "post-transition metals"),
(7, "metalloids"),
(8, "other nonmetals"),
(9, "noble gasses"),
(10, "unknown")
;

select * from element_category;