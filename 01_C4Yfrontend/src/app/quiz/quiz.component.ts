import { Component, OnInit } from '@angular/core';
import { QuizModel } from '@app/quiz/quiz.model';
import { QuizService } from '@app/quiz/quiz.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TestResult, TestResultSubmissionModel } from '@app/leaderboard/testresultsubmission.model';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss'],
})
export class QuizComponent implements OnInit {
  quizForm: FormGroup;
  message: string = '';

  constructor(private quizService: QuizService, private fb: FormBuilder) {
    this.quizForm = this.fb.group({
      userName: ['', Validators.required],
      answers: this.fb.array([]),
    });
  }

  quiz: QuizModel[] = [];

  get answers(): FormArray {
    return this.quizForm.get('answers') as FormArray;
  }

  addInputField(): void {
    this.answers.push(this.fb.control(''));
  }

  ngOnInit() {
    this.quizService.getQuiz().subscribe((quizResponse: QuizModel[]) => {
      console.log(quizResponse);
      this.quiz = quizResponse;
      this.quiz.forEach((el) => this.addInputField());
    });
  }

  submitResponse() {
    const userName: string = this.quizForm.get('userName')?.value;
    const repsonse: string[] = this.answers.value;
    console.log(repsonse);

    const resultSubmission = this.toTestResultSubmission(userName, repsonse);

    this.quizService.submitResponse(resultSubmission).subscribe((score: number) => {
      this.message = score.toString() + ' . Refresh page to redo the test';
      console.log(score);
    });
  }

  private toTestResultSubmission(userName: string, answers: string[]): TestResultSubmissionModel {
    return {
      userName: userName,
      testResult: {
        elements: this.quiz,
        answers: answers,
      } as TestResult,
    };
  }
}
