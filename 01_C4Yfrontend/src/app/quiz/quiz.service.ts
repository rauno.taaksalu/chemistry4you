import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { QuizModel } from '@app/quiz/quiz.model';
import { TestResultSubmissionModel } from '@app/leaderboard/testresultsubmission.model';

@Injectable({
  providedIn: 'root',
})
export class QuizService {
  constructor(private httpClient: HttpClient) {}

  getQuiz(): Observable<QuizModel[]> {
    return this.httpClient.get<QuizModel[]>('/quiz');
  }

  submitResponse(resultSubmission: TestResultSubmissionModel): Observable<number> {
    return this.httpClient.post<number>('/quiz', resultSubmission);
  }
}
