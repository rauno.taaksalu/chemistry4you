import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';

import { Shell } from '@app/shell/shell.service';
import { QuizComponent } from './quiz.component';

const routes: Routes = [
  Shell.childRoutes([{ path: 'quiz', component: QuizComponent, data: { title: marker('quiz') } }]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class QuizRoutingModule {}
