import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { QuizRoutingModule } from '@app/quiz/quiz-routing.module';
import { QuizComponent } from '@app/quiz/quiz.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, TranslateModule, QuizRoutingModule, ReactiveFormsModule],
  declarations: [QuizComponent],
})
export class QuizModule {}
