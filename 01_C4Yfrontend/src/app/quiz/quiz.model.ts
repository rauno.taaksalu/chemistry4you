export interface QuizModel {
  id: number;
  elementSymbol: string;
  elementName: string;
}
