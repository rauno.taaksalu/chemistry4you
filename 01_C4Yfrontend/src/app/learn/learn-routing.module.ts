import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';

import { Shell } from '@app/shell/shell.service';
import { LearnComponent } from './learn.component';

const routes: Routes = [
  Shell.childRoutes([{ path: 'learn', component: LearnComponent, data: { title: marker('learn') } }]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class LearnRoutingModule {}
