import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { LearnRoutingModule } from './learn-routing.module';
import { LearnComponent } from '@app/learn/learn.component';

@NgModule({
  imports: [CommonModule, TranslateModule, LearnRoutingModule],
  declarations: [LearnComponent],
})
export class LearnModule {}
