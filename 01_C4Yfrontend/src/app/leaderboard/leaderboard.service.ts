import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LeaderboardModel } from '@app/leaderboard/leaderboard.model';

@Injectable({
  providedIn: 'root',
})
export class LeaderboardService {
  constructor(private httpClient: HttpClient) {}

  getLeaderboard(): Observable<LeaderboardModel[]> {
    return this.httpClient.get<LeaderboardModel[]>('/api/leaderboard');
  }
}
