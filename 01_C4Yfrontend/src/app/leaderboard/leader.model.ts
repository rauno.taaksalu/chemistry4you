export interface LeaderModel {
  id: number;
  userName: string;
  score: number;
}
