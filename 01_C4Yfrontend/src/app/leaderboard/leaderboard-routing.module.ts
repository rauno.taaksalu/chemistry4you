import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';

import { Shell } from '@app/shell/shell.service';
import { LeaderboardComponent } from './leaderboard.component';

const routes: Routes = [
  Shell.childRoutes([{ path: 'leaderboard', component: LeaderboardComponent, data: { title: marker('Leaderboard') } }]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class LeaderboardRoutingModule {}
