import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { LeaderboardRoutingModule } from './leaderboard-routing.module';
import { LeaderboardComponent } from './leaderboard.component';

@NgModule({
  imports: [CommonModule, TranslateModule, LeaderboardRoutingModule],
  declarations: [LeaderboardComponent],
})
export class LeaderboardModule {}
