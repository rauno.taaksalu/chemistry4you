export interface LeaderboardModel {
  userName: string;
  score: number;
}
