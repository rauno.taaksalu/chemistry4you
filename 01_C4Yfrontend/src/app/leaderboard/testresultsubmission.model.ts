import { QuizModel } from '@app/quiz/quiz.model';

export interface TestResultSubmissionModel {
  userName: string;
  testResult: TestResult;
}

export interface TestResult {
  answers: string[];
  elements: QuizModel[];
}
