import { Component, OnInit } from '@angular/core';

import { LeaderboardModel } from '@app/leaderboard/leaderboard.model';
import { LeaderboardService } from '@app/leaderboard/leaderboard.service';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.scss'],
})
export class LeaderboardComponent implements OnInit {
  leaderboard: LeaderboardModel[] = [];

  constructor(private leaderboardService: LeaderboardService) {}
  ngOnInit() {
    this.leaderboardService.getLeaderboard().subscribe((leaderboard: LeaderboardModel[]) => {
      console.log(leaderboard);
      this.leaderboard = leaderboard;
    });
  }
}
